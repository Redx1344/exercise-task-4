<?php 
class API {
    
    # In the 1st function, put your full name as the parameter of it.
    function add_name($full_name) {
        echo 'Full Name: ' . $full_name . "\n";
    }
    # In the 2nd Function, put an Array Value of your hobbies as the parameter of it.
    function add_hobbies($hobbies) {
        echo 'Hobbies:'. "\n";
        foreach ($hobbies as $hobby) {
            echo "\t", $hobby, "\n";
        }
    }

    # In the 3rd Function, put an Object Value of your Age, email address and Birthday as the parameter of it.
    function add_info($age,$email,$birthdate) {
        echo "Age: ".$age."\n";
        echo "Email: ".$email."\n";
        echo "Birthday: ".$birthdate."\n";
    }
}

# Outside of Class, use the class and call the function you created. 
$person = new API;

# Necessary arguments for the methods
$fullname = "Whilburt Delos Santos";
$hobbies = array("Playing Computer Games", "Listening to Music", "Reading Novels");
$extraInfo = (object) [
    "age" => 23,
    "email" => "whilburtds@gmail.com",
    "birthday" => "November 25,1999"
];

# Call the object methods
$person->add_name($fullname);
$person->add_hobbies($hobbies);
$person->add_info($extraInfo->age, $extraInfo->email, $extraInfo->birthday);
?>

